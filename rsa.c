/**
  FILE:        rsa.c
  AUTHOR:      t4
  DESCRIPTION: RSA cipher declarations.
  LICENSE:     See LICENSE.
*/

#include "rsa.h"

#define RSA_BIGINT_RADIX 10

rsa_biginteger* rsa_biginteger_new(char *buffer)
{
  mp_int *bint = (mp_int*) malloc(sizeof(mp_int));
  mp_init(bint);
  if (buffer) {
    mp_read_radix(bint, buffer, RSA_BIGINT_RADIX);
  }
  return bint;
}


rsa_biginteger* rsa_biginteger_new_str(unsigned char *str, int length)
{
  mp_int *bint = (mp_int*) malloc(sizeof(mp_int));
  mp_init(bint);
  mp_read_signed_bin(bint, str, length);
  return bint;
}


rsa_biginteger* rsa_modpow(rsa_biginteger *data, rsa_biginteger *exponent,
                           rsa_biginteger *modulus)
{
  mp_int *result = (mp_int*) malloc(sizeof(mp_int));
  mp_init(result);
  mp_exptmod(data, exponent, modulus, result);
  return result;
}


void rsa_biginteger_free(rsa_biginteger *bint)
{
  mp_clear(bint);
  free(bint);
}
