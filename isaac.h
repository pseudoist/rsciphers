/**
  FILE:        isaac.h
  AUTHOR:      t4
  DESCRIPTION: ISAAC cipher declarations.
  LICENSE:     See LICENSE.
  NOTES:       ISAAC by Bob Jenkins (http://burtleburtle.net/bob/rand/isaac.html)
*/

#ifndef RSCIPHERS_ISAAC_H_
#define RSCIPHERS_ISAAC_H_

#include <stdint.h>
#include <stddef.h>

#define PRNG_ISAAC_RESULT_SIZE 256
#define PRNG_ISAAC_MEMORY_SIZE 256
#define PRNG_ISAAC_SEED_MAX_LENGTH 256
#define PRNG_ISAAC_GOLDEN_RATIO 0x9E3779B9

/** ISAAC structure */
struct prng_isaac_info {
  uint32_t randcnt;
  uint32_t aa;
  uint32_t bb;
  uint32_t cc;
  uint32_t mm[PRNG_ISAAC_MEMORY_SIZE];
  uint32_t randrsl[PRNG_ISAAC_RESULT_SIZE];
};


/**
  Creates a new ISAAC structure.
  Params:  seed   - the seed to use
           length - the length of the seed 
  Returns: ISAAC structure on success
           NULL on error           
*/
struct prng_isaac_info* prng_isaac_new(uint32_t *seed, size_t length);


/**
  Provides the next random u32.
  Params:  isaac - ISAAC info structure
  Returns: random u32         
*/
uint32_t prng_isaac_rand(struct prng_isaac_info *isaac);

#endif
