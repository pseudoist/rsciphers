/**
  FILE:        test.c
  AUTHOR:      t4
  DESCRIPTION: Test module.
  LICENSE:     See LICENSE.
*/

#include "isaac.h"
#include "rsa.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RSA_PRIVATE_EXPONENT "5619664045908831135405113830837622935618085603599503423402863983838948530196116092095605683192468571950626582779765176018884224752374913260518099520065073"
#define RSA_PUBLIC_EXPONENT "65537"
#define RSA_MODULUS "7169474840894044503037666831450365862012896169030575352531701321955483157941825374118145355691304099554818090694185579799948503371294810857419425026906919"

void test_isaac(void)
{
  uint32_t seed[2] = { 0x97, 0x56 };
  struct prng_isaac_info *isaac = prng_isaac_new(seed, 2);
  printf("%x\n", prng_isaac_rand(isaac)); //This is correct, trust me :)
  free(isaac);
}


void test_rsa(void) //TODO: bug
{
  unsigned char str[255];
  memset(str, '\0', 255);

  rsa_biginteger *priv_exp = rsa_biginteger_new(RSA_PRIVATE_EXPONENT);
  rsa_biginteger *publ_exp = rsa_biginteger_new(RSA_PUBLIC_EXPONENT);
  rsa_biginteger *modulus  = rsa_biginteger_new(RSA_MODULUS);

  rsa_biginteger *m  = rsa_biginteger_new_str("hue", 3);
  rsa_biginteger *c  = rsa_modpow(m, publ_exp, modulus);
  rsa_biginteger *m2 = rsa_modpow(c, priv_exp, modulus);

  rsa_biginteger_to_buffer(m2, str);
  printf("%s\n", str);

  rsa_biginteger_free(priv_exp);
  rsa_biginteger_free(publ_exp);
  rsa_biginteger_free(modulus);
  rsa_biginteger_free(m);
  rsa_biginteger_free(c);
  rsa_biginteger_free(m2);
}


int main(void)
{
  test_isaac();
  test_rsa();
  return 0;
}
