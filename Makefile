CC=clang
CFLAGS=-std=c99 -Wall -pedantic -g
LDFLAGS=-ltommath
EXECNAME=cipher_test
########################
DEPS = rsa.o isaac.o test.o

all: $(DEPS)
	$(CC) -o $(EXECNAME) $(DEPS) $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<;

clean:
	rm -f *.o
