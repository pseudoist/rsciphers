/**
  FILE:        rsa.h
  AUTHOR:      t4
  DESCRIPTION: RSA cipher declarations.
  LICENSE:     See LICENSE.
*/

#ifndef RSCIPHERS_RSA_H_
#define RSCIPHERS_RSA_H_

#include <tommath.h>

#define rsa_biginteger_to_buffer(bint, buffer) mp_toraw(bint, buffer)

typedef mp_int rsa_biginteger;

/**
  Creates a new Big Integer.
  Params:  buffer - string representation of value or NULL
  Returns: big integer on success
           NULL on error           
*/
rsa_biginteger* rsa_biginteger_new(char *buffer);


/**
  Creates a new Big Integer.
  Params:  buffer - string data
           length - length of string
  Returns: big integer on success
           NULL on error           
*/
rsa_biginteger* rsa_biginteger_new_str(unsigned char *str, int length);


/**
  Creates a new Big Integer.
  Params:  data     - data to use cipher on
           exponent - RSA exponent
           modulus  - RSA modulus
  Returns: big integer on success
           NULL on error           
*/
rsa_biginteger* rsa_modpow(rsa_biginteger *data, rsa_biginteger *exponent,
                           rsa_biginteger *modulus);


/**
  Frees a Big Integer.
  Params:  bint - big integer to free
*/
void rsa_biginteger_free(rsa_biginteger *bint);


#endif
