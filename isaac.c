/**
  FILE:        isaac.h
  AUTHOR:      t4
  DESCRIPTION: ISAAC cipher definitions.
  LICENSE:     See LICENSE.
  NOTES:       ISAAC by Bob Jenkins (http://burtleburtle.net/bob/rand/isaac.html)
*/

#include "isaac.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define mix(a, b, c, d, e, f, g, h) \
{ \
  a ^= b << 11; d += a; b += c; \
  b ^= c >> 2;  e += b; c += d; \
  c ^= d << 8;  f += c; d += e; \
  d ^= e >> 16; g += d; e += f; \
  e ^= f << 10; h += e; f += g; \
  f ^= g >> 4;  a += f; g += h; \
  g ^= h << 8;  b += g; h += a; \
  h ^= a >> 9;  c += h; a += b; \
}


static void prng_isaac_isaac(struct prng_isaac_info *isaac) {
  register size_t i;
  register uint32_t x, y;

  isaac->cc = isaac->cc + 1;
  isaac->bb = isaac->bb + isaac->cc;

  for (i = 0; i < PRNG_ISAAC_MEMORY_SIZE; ++i) {
    x = isaac->mm[i];
    switch (i % 4) {
      case 0: isaac->aa = isaac->aa ^ (isaac->aa << 13); break;
      case 1: isaac->aa = isaac->aa ^ (isaac->aa >> 6); break;
      case 2: isaac->aa = isaac->aa ^ (isaac->aa << 2); break;
      case 3: isaac->aa = isaac->aa ^ (isaac->aa >> 16); break;
    }
    isaac->aa = isaac->mm[(i + 128) % PRNG_ISAAC_MEMORY_SIZE] + isaac->aa;
    isaac->mm[i] = y = isaac->mm[(x >> 2) % PRNG_ISAAC_MEMORY_SIZE] + isaac->aa + isaac->bb;
    isaac->randrsl[i] = isaac->bb = isaac->mm[(y >> 10) % PRNG_ISAAC_MEMORY_SIZE] + x;
   }
}


static void prng_isaac_init(struct prng_isaac_info *isaac, int flag) {
  size_t i;
  uint32_t a, b, c, d, e, f, g, h;
  isaac->aa = isaac->bb = isaac->cc = 0;
  a = b = c = d = e = f = g = h = PRNG_ISAAC_GOLDEN_RATIO;

  for (i = 0; i < 4; ++i) {
    mix(a, b, c, d, e, f, g, h);
  }

  for (i = 0; i < PRNG_ISAAC_MEMORY_SIZE; i += 8) {
    if (flag) {
      a += isaac->randrsl[i];     b += isaac->randrsl[i + 1]; c += isaac->randrsl[i + 2]; d += isaac->randrsl[i + 3];
      e += isaac->randrsl[i + 4]; f += isaac->randrsl[i + 5]; g += isaac->randrsl[i + 6]; h += isaac->randrsl[i + 7];
    }
    mix(a, b, c, d, e, f, g, h);
    isaac->mm[i]     = a; isaac->mm[i + 1] = b; isaac->mm[i + 2] = c; isaac->mm[i + 3] = d;
    isaac->mm[i + 4] = e; isaac->mm[i + 5] = f; isaac->mm[i + 6] = g; isaac->mm[i + 7] = h;
   }

   if (flag) {
     for (i = 0; i < PRNG_ISAAC_MEMORY_SIZE; i += 8) {
       a += isaac->mm[i];     b += isaac->mm[i + 1]; c += isaac->mm[i + 2]; d += isaac->mm[i + 3];
       e += isaac->mm[i + 4]; f += isaac->mm[i + 5]; g += isaac->mm[i + 6]; h += isaac->mm[i + 7];
       mix(a, b, c, d, e, f, g, h);
       isaac->mm[i] = a;     isaac->mm[i + 1] = b; isaac->mm[i + 2] = c; isaac->mm[i + 3] = d;
       isaac->mm[i + 4] = e; isaac->mm[i + 5] = f; isaac->mm[i + 6] = g; isaac->mm[i + 7] = h;
     }
   }

  prng_isaac_isaac(isaac);
  isaac->randcnt = PRNG_ISAAC_MEMORY_SIZE;
}


struct prng_isaac_info* prng_isaac_new(uint32_t *seed, size_t length) {
  assert(length < PRNG_ISAAC_RESULT_SIZE); //Or return NULL, whatever you want.
  struct prng_isaac_info *isaac = (struct prng_isaac_info *) malloc(sizeof(struct prng_isaac_info));
  memset(isaac, 0, sizeof(struct prng_isaac_info));
  for (int i = 0; i < length; ++i) {
    isaac->randrsl[i] = seed[i];
  }
  prng_isaac_init(isaac, 1);
  return isaac;
}


uint32_t prng_isaac_rand(struct prng_isaac_info *isaac) {
  if (isaac->randcnt-- == 0) {
    prng_isaac_isaac(isaac);
    isaac->randcnt = PRNG_ISAAC_RESULT_SIZE - 1;
  }
  return isaac->randrsl[isaac->randcnt];
}
